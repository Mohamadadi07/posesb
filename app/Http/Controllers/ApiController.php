<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HeaderSO;
use App\DetailSO;
use App\Product;
use App\Customer;


class ApiController extends Controller
{
    public function getInvoiceAll(){

        $header = HeaderSo::all();
        $data=[];
        foreach($header as $key => $val){
            $data[$val->so_code]['so_code'] = $val->so_code;
            $data[$val->so_code]['posting_date'] = $val->posting_date;
            $data[$val->so_code]['delivery_date'] = $val->delivery_date;
            $data[$val->so_code]['subject'] = $val->subject;
            $data[$val->so_code]['status'] = $val->status;
            $data[$val->so_code]['tax'] = $val->tax;
            $data[$val->so_code]['shipment'] = $val->shipment;
            $data[$val->so_code]['total_payment'] = $val->total_payment;
            $data[$val->so_code]['customer_name'] = Customer::where('id',$val->id_customer)->first();
            $data[$val->so_code]['detail_order'] = DetailSO::where('so_code', $val->so_code)->get();
        }

        return response()->json($data);
    }


    public function getInvoiceParam($so_code){
        $data =[];
        $header = HeaderSo::where('so_code',$so_code)->first();
            $data['so_code'] = $header->so_code;
            $data['posting_date'] = $header->posting_date;
            $data['delivery_date'] = $header->delivery_date;
            $data['subject'] = $header->subject;
            $data['status'] = $header->status;
            $data['tax'] = $header->tax;
            $data['shipment'] = $header->shipment;
            $data['total_payment'] = $header->total_payment;
            $data['customer_name'] = Customer::where('id',$header->id_customer)->first();
            $data['detail_order'] = DetailSO::where('so_code', $header->so_code)->get();

        return response()->json($data);
    }
}
