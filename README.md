<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

### Laravel Quick Start

1.  Clone this Repository

        git clone git@gitlab01.bumn.go.id:didirosidi/jdih-v4.git

2.  To install `Composer` globally, download the installer from https://getcomposer.org/download/ Verify that Composer in successfully installed, and version of installed Composer will appear:

        composer --version

3.  Install `Composer` dependencies.

        composer install

4.  Copy `.env.example` file and create duplicate. Use `cp` command for Linux or Max user.

        cp .env.example .env

    If you are using `Windows`, use `copy` instead of `cp`.

        copy .env.example .env

5.  Comment on the line of code app\Providers\ViewComposerServiceProvider.php

        public function boot()
        {
        // $this->registerLayout();
        // $this->pageTitleLayout();
        }

6.  Create a table in MySQL/PostgreSQL database and fill the database details `DB_DATABASE` in `.env` file.

7.  The below command will create tables into database using Laravel migration and seeder.

        php artisan migrate:fresh --seed

8.  Uncomment on the line of code app\Providers\ViewComposerServiceProvider.php

        public function boot()
        {
          $this->registerLayout();
          $this->pageTitleLayout();
        }

9.  Generate your application encryption key:

        php artisan key:generate

10. Start the localhost server:

        php artisan serve