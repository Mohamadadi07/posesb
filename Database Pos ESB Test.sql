/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : PostgreSQL
 Source Server Version : 120009
 Source Host           : localhost:5432
 Source Catalog        : posesb
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120009
 File Encoding         : 65001

 Date: 06/01/2023 07:23:43
*/


-- ----------------------------
-- Sequence structure for customers_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."customers_id_seq";
CREATE SEQUENCE "public"."customers_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for detail_so_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."detail_so_id_seq";
CREATE SEQUENCE "public"."detail_so_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for failed_jobs_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."failed_jobs_id_seq";
CREATE SEQUENCE "public"."failed_jobs_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for header_so_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."header_so_id_seq";
CREATE SEQUENCE "public"."header_so_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for migrations_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."migrations_id_seq";
CREATE SEQUENCE "public"."migrations_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for products_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."products_id_seq";
CREATE SEQUENCE "public"."products_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS "public"."customers";
CREATE TABLE "public"."customers" (
  "id" int8 NOT NULL DEFAULT nextval('customers_id_seq'::regclass),
  "customer_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "address" text COLLATE "pg_catalog"."default" NOT NULL,
  "zip" text COLLATE "pg_catalog"."default" NOT NULL,
  "city" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "province" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "contact_person" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "phone" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "country" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO "public"."customers" VALUES (2, 'Barington Publish', '17 Great Suffolk Street', '0000', 'London', '-', 'Mohamad Adi', '085781271207', 'mohamadadi007@gmail.com', 'United Kingdom', '2023-01-05 20:14:04', '2023-01-05 20:14:04');

-- ----------------------------
-- Table structure for detail_so
-- ----------------------------
DROP TABLE IF EXISTS "public"."detail_so";
CREATE TABLE "public"."detail_so" (
  "id" int8 NOT NULL DEFAULT nextval('detail_so_id_seq'::regclass),
  "so_code" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "id_product" int4 NOT NULL,
  "qty" int4 NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of detail_so
-- ----------------------------
INSERT INTO "public"."detail_so" VALUES (9, 'SO-100003', 2, 50, '2023-01-05 21:49:17', '2023-01-05 21:49:17');
INSERT INTO "public"."detail_so" VALUES (13, 'SO-100002', 2, 50, '2023-01-05 21:56:06', '2023-01-05 21:56:06');
INSERT INTO "public"."detail_so" VALUES (14, 'SO-100002', 2, 10, '2023-01-05 21:56:06', '2023-01-05 21:56:06');
INSERT INTO "public"."detail_so" VALUES (18, 'SO-100001', 2, 10, '2023-01-05 22:12:37', '2023-01-05 22:12:37');
INSERT INTO "public"."detail_so" VALUES (19, 'SO-100001', 3, 20, '2023-01-05 22:12:37', '2023-01-05 22:12:37');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS "public"."failed_jobs";
CREATE TABLE "public"."failed_jobs" (
  "id" int8 NOT NULL DEFAULT nextval('failed_jobs_id_seq'::regclass),
  "connection" text COLLATE "pg_catalog"."default" NOT NULL,
  "queue" text COLLATE "pg_catalog"."default" NOT NULL,
  "payload" text COLLATE "pg_catalog"."default" NOT NULL,
  "exception" text COLLATE "pg_catalog"."default" NOT NULL,
  "failed_at" timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP
)
;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for header_so
-- ----------------------------
DROP TABLE IF EXISTS "public"."header_so";
CREATE TABLE "public"."header_so" (
  "id" int8 NOT NULL DEFAULT nextval('header_so_id_seq'::regclass),
  "so_code" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "id_customer" int4 NOT NULL,
  "posting_date" date NOT NULL,
  "delivery_date" date NOT NULL,
  "subject" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "status" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "tax" int4 NOT NULL,
  "shipment" int4 NOT NULL,
  "total_payment" float4 NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of header_so
-- ----------------------------
INSERT INTO "public"."header_so" VALUES (5, 'SO-100002', 2, '2023-01-05', '2023-01-05', 'Spring Marketing Campaign', 'Open', 10, 0, 12650, '2023-01-05 21:08:37', '2023-01-05 21:48:46');
INSERT INTO "public"."header_so" VALUES (6, 'SO-100003', 2, '2023-01-06', '2023-01-06', 'Spring Marketing Campaign', 'Open', 10, 0, 12650, '2023-01-05 21:21:21', '2023-01-05 21:49:17');
INSERT INTO "public"."header_so" VALUES (4, 'SO-100001', 2, '2023-01-07', '2023-01-07', 'Spring Marketing Campaign', 'Open', 10, 0, 2530, '2023-01-05 20:26:17', '2023-01-05 22:11:47');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS "public"."migrations";
CREATE TABLE "public"."migrations" (
  "id" int4 NOT NULL DEFAULT nextval('migrations_id_seq'::regclass),
  "migration" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "batch" int4 NOT NULL
)
;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO "public"."migrations" VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO "public"."migrations" VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO "public"."migrations" VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO "public"."migrations" VALUES (4, '2020_07_06_025622_create_products_table', 1);
INSERT INTO "public"."migrations" VALUES (5, '2020_07_06_051356_create_customers_table', 1);
INSERT INTO "public"."migrations" VALUES (6, '2020_07_06_055917_create_header_s_o_s_table', 1);
INSERT INTO "public"."migrations" VALUES (7, '2020_07_06_060833_create_detail_s_o_s_table', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS "public"."password_resets";
CREATE TABLE "public"."password_resets" (
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "token" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0)
)
;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS "public"."products";
CREATE TABLE "public"."products" (
  "id" int8 NOT NULL DEFAULT nextval('products_id_seq'::regclass),
  "product_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "item_type" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "price" int4 NOT NULL,
  "diskon_price" int4 NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO "public"."products" VALUES (2, 'Design', 'Service', 230, 0, '2023-01-05 20:07:09', '2023-01-05 20:07:09');
INSERT INTO "public"."products" VALUES (3, 'Development', 'Service', 330, 0, '2023-01-05 20:07:20', '2023-01-05 20:07:20');
INSERT INTO "public"."products" VALUES (4, 'Meetings', 'Service', 60, 0, '2023-01-05 20:07:34', '2023-01-05 20:07:34');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int8 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email_verified_at" timestamp(0),
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "remember_token" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (1, 'Mohamad Adi', 'mohamadadi007@gmail.com', NULL, '$2y$10$/oOCbl3Tqo/KqtZICYee2uxB66fxxugamvjRSZQzwtX26LsHrwF8C', NULL, '2023-01-05 20:02:57', '2023-01-05 20:02:57');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."customers_id_seq"
OWNED BY "public"."customers"."id";
SELECT setval('"public"."customers_id_seq"', 2, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."detail_so_id_seq"
OWNED BY "public"."detail_so"."id";
SELECT setval('"public"."detail_so_id_seq"', 19, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."failed_jobs_id_seq"
OWNED BY "public"."failed_jobs"."id";
SELECT setval('"public"."failed_jobs_id_seq"', 1, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."header_so_id_seq"
OWNED BY "public"."header_so"."id";
SELECT setval('"public"."header_so_id_seq"', 8, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."migrations_id_seq"
OWNED BY "public"."migrations"."id";
SELECT setval('"public"."migrations_id_seq"', 7, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."products_id_seq"
OWNED BY "public"."products"."id";
SELECT setval('"public"."products_id_seq"', 4, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."users_id_seq"
OWNED BY "public"."users"."id";
SELECT setval('"public"."users_id_seq"', 1, true);

-- ----------------------------
-- Primary Key structure for table customers
-- ----------------------------
ALTER TABLE "public"."customers" ADD CONSTRAINT "customers_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table detail_so
-- ----------------------------
ALTER TABLE "public"."detail_so" ADD CONSTRAINT "detail_so_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table failed_jobs
-- ----------------------------
ALTER TABLE "public"."failed_jobs" ADD CONSTRAINT "failed_jobs_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table header_so
-- ----------------------------
ALTER TABLE "public"."header_so" ADD CONSTRAINT "header_so_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table migrations
-- ----------------------------
ALTER TABLE "public"."migrations" ADD CONSTRAINT "migrations_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table password_resets
-- ----------------------------
CREATE INDEX "password_resets_email_index" ON "public"."password_resets" USING btree (
  "email" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table products
-- ----------------------------
ALTER TABLE "public"."products" ADD CONSTRAINT "products_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_email_unique" UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
