@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><strong>Create Sales Order</strong></div>

                <div class="card-body">

                    <form action="{{route('so.update',$header->so_code)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <strong>Customer Name</strong>
                                    <select name="id_customer" id="id_customer" class="form-control">
                                        @foreach ($customers as $item)
                                        <option value="{{$item->id}}" @if($header->id_customer == $item->id) selected @endif>{{$item->customer_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <strong>Contact Person</strong>
                                    <input type="text" name="contact_person" id="contact_person" class="form-control" value="{{$customer->contact_person}}"
                                        readonly>
                                </div>
                                <div class="form-group">
                                    <strong>Email</strong>
                                    <input type="email" name="email" id="email" class="form-control"  value="{{$customer->email}}"readonly>
                                </div>
                                <div class="form-group">
                                    <strong>Address</strong>
                                    <textarea name="address" id="address" cols="30" rows="3" class="form-control"
                                        readonly>{{$customer->address}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <strong>Subject</strong>
                                    <input type="text" name="subject" id="subject" class="form-control"
                                        value="{{$header->subject}}">
                                </div>
                                <div class="form-group">
                                    <strong>Posting Date</strong>
                                    <input type="date" name="posting_date" id="posting_date" class="form-control"
                                        value="{{$header->posting_date}}">
                                </div>
                                <div class="form-group">
                                    <strong>Delivery Date</strong>
                                    <input type="date" name="delivery_date" id="delivery_date" class="form-control"
                                        value="{{$header->delivery_date}}">
                                </div>
                                <div class="form-group">
                                    <strong>Status</strong>
                                    <input type="text" name="status" id="status" class="form-control" value="Open"
                                        readonly>
                                </div>
                            </div>

                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-success btn-sm add-product" >Add Product</button>
                        </div>
                        <br>
                        <br>
                        <div class="table-responsive">
                        <table class="table table-bordered" id="product-table">
                            <thead>
                                <tr>
                                    <th scope="col" class="col-">Itey Type</th>
                                    <th scope="col" class="col-">Product Name</th>
                                    <th scope="col" class="col-">Price</th>
                                    <th scope="col" class="col-">Qty</th>
                                    <th scope="col" class="col-">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($product as $p)
                                    <tr>
                                        <td><input type='hidden' name='id_product[]' value="{{$p->id}}" readonly/><input type='hidden' name='item_type[]' value="{{$p->item_type}}" readonly/>{{ $p->item_type }}</td>
                                        <td><input type='hidden' name='product_name[]' value="{{$p->product_name}}" readonly/>{{ $p->product_name }}</td>
                                        <td><input type='hidden' name='price[]' class="price" value="{{$p->price}}" readonly/>{{ $p->price }}</td>
                                        <td><input type='hidden' name='quantity[]' class="quantity" value="{{$p->qty}}" readonly/>{{ $p->qty }}</td>
                                        <td class="text-center">
                                            <span class="btn btn-delete btn-icon-only red-mint tooltips" data-original-title="Hapus">Delete</span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                        <div class="pull-right">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Sub Total</th>
                                        <br>
                                        <th><input type="number" name="sub_total" id="sub_total" class="form-control" readonly>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><strong> Tax (%)</strong></td>
                                        <td><input type="number" name="tax" id="tax" class="form-control" value="{{$header->tax}}"></td>
                                    </tr>
                                    <tr>
                                        <td><strong> Shipment</strong></td>
                                        <td><input type="number" name="shipment" id="shipment" class="form-control" value="{{$header->shipment}}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong> Total Payment </strong></td>
                                        <td><input type="number" name="total_payment" id="total_payment" class="form-control" readonly></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <a href="{{route('so.index')}}" class="btn btn-warning">Back</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="addProduct" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <strong>Product Name</strong>
                    <select name="id_product" id="id_product" class="form-control" style="width: 100%;">
                        <option value="">Select Product</option>
                        @foreach ($products as $item)
                        <option value="{{$item->id}}">{{$item->product_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <strong>Item Type</strong>
                    <input type="text" name="item_type" id="item_type" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <strong>Price</strong>
                    <input type="number" name="price" id="price" class="form-control" readonly>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addProduct()">Save changes</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).ready(function(){
        subTotalEdit();
        grandTotalEdit();
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });
    $("#id_customer").select2({
        placeholder: 'Select Customer',
        language: 'id',
        allowClear: true
    });
    $("#id_product").select2({
        placeholder: 'Select Product',
        language: 'id',
        allowClear: true,
        dropdownParent: $("#addProduct")
    });
    $('.add-product').click(function(){
        var id_customer = document.getElementById("id_customer");
        var posting_date = document.getElementById("posting_date");
        var delivery_date = document.getElementById("delivery_date");
        if (id_customer.value != "" && posting_date.value != "" && delivery_date.value != "") {
            $('#addProduct').modal({
			    backdrop: 'static'
		    });
        }else{
            alert('Please complete input field');
        }
    });
    function addProduct(){
        id_product = document.getElementById("id_product").value;
        product_name = $("#id_product option:selected" ).text();
        var item_type = document.getElementById("item_type");
        var price = document.getElementById("price");
        $("#product-table > tbody").append("<tr><td class='col-'><input type='hidden' name='id_product[]' value="+id_product+" readonly/><input type='text' name='product_name[]' value='"+product_name+"' readonly/></td><td class='col-'><input type='text' name='item_type[]' value='"+item_type.value+"' readonly></td><td class='col-'><input type='number' class='price' name='price[]' value='"+price.value+"' readonly/></td><td class='col-'><input type='number' class='quantity' id='quantity' name='quantity[]'></td><td class='col-'><button class='btn btn-sm btn-danger btn-delete'>Delete</button></td></tr>");
        document.getElementById("item_type").value ="";
        document.getElementById("price").value ="";
        $('#addProduct').modal('toggle');
    }
    function subTotal(){
        var sum = 0.0;
        $('#product-table > tbody  > tr').each(function() {
            var qty = $(this).find('.quantity').val();
            var price = $(this).find('.price').val();
            var amount = parseFloat(qty) * parseFloat(price);
            amount = isNaN(amount) ? 0 : amount;  //checks for initial empty text boxes
            sum += amount;
        });
        $("#sub_total").val(sum);
    }
    function subTotalEdit(){
        var sum = 0.0;
        $('#product-table > tbody  > tr').each(function() {
            var qty = $(this).find('.quantity').val();
            var price = $(this).find('.price').val();
            var amount = parseFloat(qty) * parseFloat(price);
            amount = isNaN(amount) ? 0 : amount;  //checks for initial empty text boxes
            sum += amount;
        });
        $("#sub_total").val(sum);
    }
    function grandTotalEdit(){
        var tax = $("#tax").val();
        var subTotal = $("#sub_total").val();
        var shipment = $("#shipment").val();
        tax = isNaN(parseFloat(tax)) ? 0 : tax;
        shipment = isNaN(parseFloat(shipment)) ? 0 : shipment;
        var calTax = (tax / 100) * subTotal;
        var total_payment = parseFloat(subTotal) + parseFloat(calTax) + parseFloat(shipment);
        $("#total_payment").val(total_payment);
    }
    function grandTotal(){
        var tax = $("#tax").val();
        var subTotal = $("#sub_total").val();
        var shipment = $("#shipment").val();
        tax = isNaN(parseFloat(tax)) ? 0 : tax;
        shipment = isNaN(parseFloat(shipment)) ? 0 : shipment;
        var calTax = (tax / 100) * subTotal;
        var total_payment = parseFloat(subTotal) + parseFloat(calTax) + parseFloat(shipment);
        $("#total_payment").val(total_payment);
    }
    $(document).on('change','#quantity',function(){
     subTotal();
    });
    
    $("#id_customer").change(function(){
        const id_customer = $(this).val();
        $.ajax({
                url: "{{route('getCustomer')}}",
                type: "get",
                dataType: "json",
                async: true,
                data: {
                    id_customer: id_customer
                },
                success: function(data){
                    if(data.id === undefined){
                        $('#contact_person').val('');
                        $('#email').val('');
                        $('#address').val('');
                    }else{
                        $('#contact_person').val(data.contact_person);
                        $('#email').val(data.email);
                        $('#address').val(data.address);
                    }
                }     
            });  
    });
    
    $("#id_product").change(function(){
        const id_product = $(this).val();
        $.ajax({
                url: "{{route('getProduct')}}",
                type: "get",
                dataType: "json",
                async: true,
                data: {
                    id_product: id_product
                },
                success: function(data){
                    if(data.id === undefined){
                        $('#item_type').val('');
                        $('#price').val('');
                    }else{
                        $('#item_type').val(data.item_type);
                        $('#price').val(data.price);
                    }
                }     
            });  
    });
    $("#tax").change(function(){
        grandTotal();
    })
    $("#shipment").change(function(){
        grandTotal();
    })
    $("body").on("click", ".btn-delete", function(){
        $(this).parents("tr").remove();
        subTotal();
    });

    
    
</script>
@endpush
