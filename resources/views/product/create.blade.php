@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><strong>Create Product</strong></div>

                <div class="card-body">
                  
                    <form action="{{route('product.store')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <strong>Product Name</strong>
                            <input type="text" name="product_name" id="" value="{{old('product_name')}}"
                                class="form-control">
                        </div>
                        <div class="form-group">
                            <strong>Unit</strong>
                            <select name="item_type" id="" class="form-control">
                                <option value="Service">Service</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <strong>Price</strong>
                            <input type="number" name="price" id="" class="form-control" value="{{old('price')}}">
                        </div>
                        <div class="form-group">
                            <strong>Diskon Price</strong>
                            <input type="number" name="diskon_price" id="" class="form-control"
                                value="{{old('diskon_price')}}">
                        </div>

                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <a href="{{route('product.index')}}" class="btn btn-warning">Back</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
